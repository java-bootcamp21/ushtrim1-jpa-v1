package com.jpa.service;

import java.util.List;

import com.jpa.entity.Posts;
import com.jpa.entity.Users;

public interface UserService {

	List<Users> getUsers();
	Users getUserById(Integer id);
	Users addUser(Users user);
	Users updateUser(Integer id,Users user);
	void deleteUser(Integer id);
	List<Posts> getUserPosts(Users user);
	List<Posts> getUserPostsById(Integer userId);
	Users addUserPost(Integer userId ,Posts post);
	void deleteUserPost(Integer userId);
}
