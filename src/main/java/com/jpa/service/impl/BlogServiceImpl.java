package com.jpa.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.jpa.entity.Categories;
import com.jpa.entity.PostCategories;
import com.jpa.entity.Posts;
import com.jpa.repository.CategoryRepository;
import com.jpa.repository.PostCategoriesRepository;
import com.jpa.repository.PostsRepository;
import com.jpa.service.BlogService;

@Service
public class BlogServiceImpl implements BlogService{

	private final PostCategoriesRepository postCategoriesRepository;
	private final CategoryRepository categoryRepository;
	private final PostsRepository postsRepository;
	
	
	public BlogServiceImpl(PostCategoriesRepository postCategoriesRepository, CategoryRepository categoryRepository,
			PostsRepository postsRepository) {
		super();
		this.postCategoriesRepository = postCategoriesRepository;
		this.categoryRepository = categoryRepository;
		this.postsRepository = postsRepository;
	}

	@Override
	public List<PostCategories> getPostCategories() {
		return postCategoriesRepository.findAll();
	}

	@Override
	public PostCategories getPostCategoriesById(Integer id) {
		return postCategoriesRepository.findById(id).orElse(null);
	}

	@Override
	public Categories addCategories(Categories categories) {
		return categoryRepository.save(categories);
	}

	@Override
	public void deleteCategories(Integer id) {
		categoryRepository.deleteById(id);
	}

	@Override
	public List<Posts> getPosts() {
		return postsRepository.findAll();
	}

	@Override
	public List<Posts> getPostsByCategory(Categories category) {
		
		return postsRepository.getPostsByPostCategories_categories(category);
	}

	@Override
	public void deletePost(Integer id) {
		postsRepository.deleteById(id);
	}

	
	
	
	
}
