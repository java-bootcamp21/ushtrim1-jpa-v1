package com.jpa.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;


import com.jpa.entity.Posts;
import com.jpa.entity.Users;
import com.jpa.repository.PostsRepository;
import com.jpa.repository.UsersRepository;
import com.jpa.service.UserService;

@Service
public class UserServiceImpl implements UserService{
	
	private final UsersRepository usersRepository;
	private final PostsRepository postsRepository;


	public UserServiceImpl(UsersRepository usersRepository, PostsRepository postsRepository) {
		super();
		this.usersRepository = usersRepository;
		this.postsRepository = postsRepository;
	}


	@Override
	public List<Users> getUsers() {
		return usersRepository.findAll();
	}


	@Override
	public Users getUserById(Integer id) {
		return usersRepository.findById(id).orElse(null);
	}


	@Override
	public Users addUser(Users user) {
		return usersRepository.save(user);
	}


	@Override
	public Users updateUser(Integer id, Users user) {
		
		Users u1 = getUserById(id);
		u1.setUsername(user.getUsername());
		u1.setEmail(user.getUsername());
		u1.setPassword(user.getPassword());
		u1.setDateModified(user.getDateModified());
		return usersRepository.save(u1) ;
	}


	@Override
	public void deleteUser(Integer id) {
		usersRepository.deleteById(id);
	}


	@Override
	public List<Posts> getUserPosts(Users users) {
		return postsRepository.findAllPostsByUsers(users);
	}

	@Override
	public List<Posts> getUserPostsById(Integer userId) {
		return postsRepository.getPostsByUsers_id(userId);
	}


	@Override
	public Users addUserPost(Integer userId, Posts post) {
		usersRepository.findById(userId).get().getPosts().add(post);
		return usersRepository.findById(userId).orElse(null);
	}


	@Override
	public void deleteUserPost(Integer userId) {
		usersRepository.findById(userId).get().getPosts().clear();
		
	}

}
