package com.jpa.service;

import java.util.List;

import com.jpa.entity.Categories;
import com.jpa.entity.PostCategories;
import com.jpa.entity.Posts;

public interface BlogService {

	List<PostCategories> getPostCategories();
	PostCategories getPostCategoriesById(Integer id);
	Categories addCategories(Categories categories);
	void deleteCategories(Integer id);
	List<Posts> getPosts();
	List<Posts> getPostsByCategory(Categories categoriy);
	void deletePost(Integer id);
	
}
