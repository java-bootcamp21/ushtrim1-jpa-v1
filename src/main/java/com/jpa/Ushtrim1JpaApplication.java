package com.jpa;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.jpa.entity.Categories;
import com.jpa.entity.PostCategories;
import com.jpa.entity.Posts;
import com.jpa.entity.Users;
import com.jpa.repository.CategoryRepository;
import com.jpa.repository.PostCategoriesRepository;
import com.jpa.repository.PostsRepository;
import com.jpa.repository.UsersRepository;
import com.jpa.service.BlogService;
import com.jpa.service.UserService;

@SpringBootApplication
public class Ushtrim1JpaApplication implements CommandLineRunner {

	Logger logger = LoggerFactory.getLogger(Ushtrim1JpaApplication.class);
	
	private final BlogService blogService;
	private final UserService userService;
	private final UsersRepository usersRepository;
	private final PostsRepository postsRepository;
	private final PostCategoriesRepository postCategoriesRepository;
	private final CategoryRepository categoryRepository;
	
	
	public Ushtrim1JpaApplication(BlogService blogService, UserService userService, UsersRepository usersRepository,
			PostsRepository postsRepository, PostCategoriesRepository postCategoriesRepository,
			CategoryRepository categoryRepository) {
		super();
		this.blogService = blogService;
		this.userService = userService;
		this.usersRepository = usersRepository;
		this.postsRepository = postsRepository;
		this.postCategoriesRepository = postCategoriesRepository;
		this.categoryRepository = categoryRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(Ushtrim1JpaApplication.class, args);
		
	}

	@Override
	public void run(String... args) throws Exception {
		
		usersRepository.deleteAll();
		postCategoriesRepository.deleteAll();
		postsRepository.deleteAll();
		categoryRepository.deleteAll();
		
		//================================================
		
		Users u1 = new Users();
		u1.setUsername("user111");
		u1.setEmail("user111@gmail.com");
		u1 = userService.addUser(u1);
		
		Users u2 = new Users();
		u2.setUsername("user2222");
		u2.setEmail("user222@gmail.com");
		u2 = userService.addUser(u2);
		
		logger.info("List all users: {}", userService.getUsers());
		
		//================================
		
		Categories c1 = new Categories();
		c1.setName("category tester");
		c1 = blogService.addCategories(c1);
		
		logger.info("categories {}", categoryRepository.findAll());
		
	//============================================
		
		Posts p1 = new Posts();
		p1.setTitle("title tester");
		p1.setBody("body tester");
		p1.setUsers(u1);
		p1= postsRepository.save(p1);
		
		logger.info("posts: {}", blogService.getPosts());
		logger.info("List all users: {}", userService.getUsers());
		
		//=========================================
		
		PostCategories pc = new PostCategories();
		pc.setCategories(c1);
		pc.setPosts(p1);
		pc=postCategoriesRepository.save(pc);
		
		var posts = blogService.getPosts();
		
		posts.stream().peek(p-> {
			logger.info("post with id {} and body {} with username {}",p.getId(),p.getUsers().getUsername());
		});
		
		logger.info("post categories: {}", postCategoriesRepository.findAll());
		
		logger.info("get user posts: {}", userService.getUserPosts(u1));
		logger.info("get user posts by user id: {}", userService.getUserPostsById(u1.getId()));
		
		logger.info("get posts by category: {}", postsRepository.getPostsByPostCategories_categories(c1));
	}

}
