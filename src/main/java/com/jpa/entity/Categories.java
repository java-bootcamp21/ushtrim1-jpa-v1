package com.jpa.entity;

import java.time.LocalDateTime;
import java.util.List;


import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity
public class Categories extends BaseEntity<Integer>{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String name;
	private LocalDateTime dateCreated;
	private LocalDateTime dateModified;
	
	@OneToMany(mappedBy = "categories",cascade = CascadeType.ALL)
	private List<PostCategories> postCategories;
	
	
	public List<PostCategories> getPostCategories() {
		return postCategories;
	}



	public void setPostCategories(List<PostCategories> postCategories) {
		this.postCategories = postCategories;
	}



	public Categories() {
		super();
	}



	public Categories(String name, LocalDateTime dateCreated, LocalDateTime dateModified) {
		super();
		this.name = name;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public LocalDateTime getDateCreated() {
		return dateCreated;
	}



	public void setDateCreated(LocalDateTime dateCreated) {
		this.dateCreated = dateCreated;
	}



	public LocalDateTime getDateModified() {
		return dateModified;
	}



	public void setDateModified(LocalDateTime dateModified) {
		this.dateModified = dateModified;
	}



	public void setId(Integer id) {
		this.id = id;
	}




	@Override
	public String toString() {
		return "Categories [id=" + id + ", name=" + name + ", dateCreated=" + dateCreated + ", dateModified="
				+ dateModified + "]";
	}



	@Override
	public Integer getId() {
		return id;
	}

}
