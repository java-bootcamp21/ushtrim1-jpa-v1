package com.jpa.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

@Entity
public class Posts extends BaseEntity<Integer>{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String title;
	private String body;
	private LocalDateTime dateCreated;
	private LocalDateTime dateModified;
	
	//lidhje shume me nje me tabelen users
	@ManyToOne
	@JoinColumn(name="user_id",referencedColumnName ="id")
	private Users users;

	//lidhje nje me shume me tabelen post_categories
	@OneToMany(mappedBy = "posts",cascade = CascadeType.ALL)
	private List<PostCategories> postCategories = new ArrayList<>();
	
	

	public List<PostCategories> getPostCategories() {
		return postCategories;
	}

	public void setPostCategories(List<PostCategories> postCategories) {
		this.postCategories = postCategories;
	}

	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	public Posts() {
		super();
	}
	
	public Posts(String title, String body, LocalDateTime dateCreated, LocalDateTime dateModified) {
		super();
		this.title = title;
		this.body = body;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
	}




	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}


	public LocalDateTime getDateCreated() {
		return dateCreated;
	}


	public void setDateCreated(LocalDateTime dateCreated) {
		this.dateCreated = dateCreated;
	}

	public LocalDateTime getDateModified() {
		return dateModified;
	}

	public void setDateModified(LocalDateTime dateModified) {
		this.dateModified = dateModified;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	

	

	@Override
	public String toString() {
		return "Posts [id=" + id + ", title=" + title + ", body=" + body + ", userId=" + this.users.getId() + ", dateCreated="
				+ dateCreated + ", dateModified=" + dateModified + "]";
	}

	@Override
	public Integer getId() {
		return id;
	}
}
