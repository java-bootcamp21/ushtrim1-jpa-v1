package com.jpa.entity;


import java.time.LocalDateTime;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import java.util.ArrayList;
@Entity
public class Users extends BaseEntity<Integer> {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String username;
	private String email;
	private String password;
	private LocalDateTime dateCreated;
	private LocalDateTime dateModified;
	
	
	
	//Lidhje nje me shume me tabelen posts
	@OneToMany(mappedBy = "users",cascade = CascadeType.ALL)
	private List<Posts> posts = new ArrayList<>();
	
	
	public List<Posts> getPosts() {
		return posts;
	}

	public void setPosts(List<Posts> posts) {
		this.posts = posts;
	}

	public Users() {
		
	}

	public Users(String username, String email, String password, LocalDateTime dateCreated,
			LocalDateTime dateModified) {
		super();
		this.username = username;
		this.email = email;
		this.password = password;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LocalDateTime getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(LocalDateTime dateCreated) {
		this.dateCreated = dateCreated;
	}

	public LocalDateTime getDateModified() {
		return dateModified;
	}

	public void setDateModified(LocalDateTime dateModified) {
		this.dateModified = dateModified;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Users [id=" + id + ", username=" + username + ", email=" + email + ", password=" + password
				+ ", dateCreated=" + dateCreated + ", dateModified=" + dateModified + "]";
	}


}
