package com.jpa.entity;

import java.time.LocalDateTime;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

@Entity
public class PostCategories extends BaseEntity<Integer>{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private LocalDateTime dateCreated;
	private LocalDateTime dateModified;
	
	@ManyToOne
	@JoinColumn(name="post_id",referencedColumnName = "id")
	private Posts posts;
	
	@ManyToOne
	@JoinColumn(name = "category_id", referencedColumnName = "id")
	private Categories categories;
	
	public Categories getCategories() {
		return categories;
	}


	public void setCategories(Categories categories) {
		this.categories = categories;
	}


	public Posts getPosts() {
		return posts;
	}


	public void setPosts(Posts posts) {
		this.posts = posts;
	}


	public PostCategories() {
		super();
	}
	
	
	public PostCategories(LocalDateTime dateCreated, LocalDateTime dateModified) {
		super();
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
	}


	public LocalDateTime getDateCreated() {
		return dateCreated;
	}


	public void setDateCreated(LocalDateTime dateCreated) {
		this.dateCreated = dateCreated;
	}


	public LocalDateTime getDateModified() {
		return dateModified;
	}


	public void setDateModified(LocalDateTime dateModified) {
		this.dateModified = dateModified;
	}


	public void setId(Integer id) {
		this.id = id;
	}



	@Override
	public String toString() {
		return "PostCategories [id=" + id + ", postId=" + this.posts.getId() + ", categoryId=" + this.categories.getId() + ", dateCreated="
				+ dateCreated + ", dateModified=" + dateModified + "]";
	}


	@Override
	public Integer getId() {
		return id;
	}
}
