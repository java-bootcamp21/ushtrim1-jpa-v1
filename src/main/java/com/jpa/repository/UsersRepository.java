package com.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jpa.entity.Posts;
import com.jpa.entity.Users;
import java.util.List;


@Repository
public interface UsersRepository extends JpaRepository<Users, Integer>{
	
}
