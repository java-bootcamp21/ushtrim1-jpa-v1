package com.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jpa.entity.Posts;
import com.jpa.entity.Users;

import java.util.List;

import com.jpa.entity.Categories;
import com.jpa.entity.PostCategories;



@Repository
public interface PostsRepository extends JpaRepository<Posts, Integer>{

	List<Posts> findAllPostsByUsers(Users users);
	
	List<Posts> getPostsByUsers_id(Integer userId);
	void deletePostsByUsers(Users users);
	
	List<Posts> getPostsByPostCategories_categories(Categories categories);
	
	
}
