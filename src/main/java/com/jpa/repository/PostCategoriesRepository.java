package com.jpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.jpa.entity.Categories;
import com.jpa.entity.PostCategories;
import com.jpa.entity.Posts;


@Repository
public interface PostCategoriesRepository extends JpaRepository<PostCategories, Integer>{
	
}
